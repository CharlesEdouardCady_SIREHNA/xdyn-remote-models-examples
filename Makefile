.PHONY: all clean

all:
	make -C force-model
	make -C wave-model
	make -C controller

clean:
	make -C force-model clean
	make -C wave-model clean
	make -C controller clean
